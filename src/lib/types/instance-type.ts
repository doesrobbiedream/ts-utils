import { Type } from './type.util'

export type ExtractFromType<T> = T extends Type<infer C> ? C : never
