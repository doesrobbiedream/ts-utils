export type TupleToUnion<ArrayType> = ArrayType extends readonly [
  infer Head,
  ...infer Rest
]
  ? Head | TupleToUnion<Rest>
  : never
