export interface Type<T = unknown> extends Function {
  new (...args: any[]): T
}

export interface AbstractType<T> extends Function {
  prototype: T
}
