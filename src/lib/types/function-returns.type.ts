export type FunctionReturns<Fn> = Fn extends (...args: any[]) => infer P
  ? P
  : never
