export type AnyOf<T> = { [K in keyof T]?: T[K] }
export type AnyObject = { [key: string]: unknown }
export type AnyFunction<
  A extends Array<unknown> = Array<unknown>,
  R = unknown
> = (...args: A) => R
