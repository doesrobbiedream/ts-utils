export type FunctionArguments<Fn> = Fn extends (...args: infer P) => any
  ? P
  : never
