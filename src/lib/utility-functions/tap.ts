import { AnyFunction } from '../types/any-like.type'

export function tap<T>(action: AnyFunction<[T]>) {
  return (argument: T): T => {
    action(argument)
    return argument
  }
}

export function tapAsync<T>(action: AnyFunction) {
  return async (argument: T): Promise<T> => {
    await action(argument)
    return argument
  }
}
