import { isOfType } from './is-of-type'

describe(isOfType.name, function () {
  describe('when checking any primitive types', function () {
    const isNumber = isOfType(Number)
    const isString = isOfType(String)
    const isBoolean = isOfType(Boolean)
    const isNull = isOfType(null)
    const isUndefined = isOfType(undefined)

    it('should check the values properly', () => {
      expect(isNumber(5)).toBeTruthy()
      expect(isNumber('5')).toBeFalsy()
      expect(isString('5')).toBeTruthy()
      expect(isString(5)).toBeFalsy()
      expect(isBoolean(true)).toBeTruthy()
      expect(isBoolean('HELLO')).toBeFalsy()
      expect(isNull(null)).toBeTruthy()
      expect(isNull('Hello')).toBeFalsy()
      expect(isUndefined(undefined)).toBeTruthy()
      expect(isUndefined('I am undefined')).toBeFalsy()
    })
  })
  describe('when checking non-primitive types', function () {
    class MyControlType {}

    class MyTestType {}

    const isOfMyControlType = isOfType(MyControlType)
    it('should check the type', () => {
      expect(isOfMyControlType(new MyTestType())).toBeFalsy()
      expect(isOfMyControlType(new MyControlType())).toBeTruthy()
    })
  })
})
