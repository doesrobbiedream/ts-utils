import { Type } from '../types/type.util'

export function isOfType<T>(type: Type | string | number) {
  return function (value: unknown): value is T {
    return (
      (type === String && typeof value === 'string') ||
      (type === Number && typeof value === 'number') ||
      (type === Boolean && typeof value === 'boolean') ||
      (type === undefined && typeof value === 'undefined') ||
      (type === null && value === null) ||
      ((value as Type<unknown>).constructor && (value as Type<unknown>).constructor === type)
    )
  }
}
