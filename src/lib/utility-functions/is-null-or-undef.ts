export function isNullOrUndef(value: unknown) {
  return value === null || typeof value === 'undefined'
}
