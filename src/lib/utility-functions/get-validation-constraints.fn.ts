import { ValidationError } from 'class-validator'

export function getValidationConstraints(
  forKey: string,
  validatedEntity: ValidationError[]
) {
  return (
    validatedEntity.find(({ property }) => property === forKey)?.constraints ||
    {}
  )
}
