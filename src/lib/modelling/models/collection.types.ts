export interface Collection<Index, Item, Raw> {
  create(item: Raw): Item

  serialize(): Item[]

  populate(values: Raw[]): void

  clear(): void

  add(item: Item): void

  remove(id: Index): void

  get(id: Index): Item | undefined
}
