import { Transform, TransformationType } from 'class-transformer'
import { isFunction } from 'radash'
import { TransformFnParams } from 'class-transformer/types/interfaces'
import { Serializable } from './serializable.types'

interface SerializerTransformParams extends TransformFnParams {
  value: Serializable<unknown>
}

export function WithSerializer(serializerKey = 'serialize') {
  return Transform(
    ({ value: serializable, type, key, obj }: SerializerTransformParams) => {
      if (type === TransformationType.CLASS_TO_PLAIN) {
        if (!isFunction((serializable as any)[serializerKey])) {
          throw Error(`Property ${key} of type ${obj.constructor.name}`)
        }
        return (serializable as any)[serializerKey]()
      }
    },
    { toPlainOnly: true }
  )
}
