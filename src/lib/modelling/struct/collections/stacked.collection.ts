import { Set } from 'typescript-collections'
import { Collection } from '../../models/collection.types';

export abstract class StackedCollection<T, R>
  extends Set<T>
  implements Collection<T, T, R>
{
  abstract create(raw: R): T

  override add(item: T): boolean {
    return super.add(item)
  }

  get(id: T): T | undefined {
    return super.dictionary.getValue(id)
  }

  populate(items: R[]): void {
    this.clear()
    items.forEach((item) => this.add(this.create(item)))
  }

  serialize(): T[] {
    return this.toArray()
  }
}
