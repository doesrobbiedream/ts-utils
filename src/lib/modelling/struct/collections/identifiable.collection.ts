import { Dictionary } from 'typescript-collections'
import { Collection, Identifiable } from '@doesrobbiedream/ts-utils'

export abstract class IdentifiableCollection<T extends Identifiable, R>
  extends Dictionary<string, T>
  implements Collection<string, T, R>
{
  abstract create(raw: R): T

  add(item: T): T | undefined {
    return super.setValue(item.uuid, item)
  }

  get(id: string): T | undefined {
    return super.getValue(id)
  }

  populate(rawItems: R[]): void {
    this.clear()
    rawItems.forEach((rawItem) => this.create(rawItem))
  }

  serialize() {
    return this.values()
  }
}
