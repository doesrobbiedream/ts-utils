import { Exclude, instanceToPlain } from 'class-transformer'
import { validate, ValidationError } from 'class-validator'
import { Serializable } from './serializable.types'
import { Validatable } from './validatable.types'
import { tap } from '@doesrobbiedream/ts-utils'

export abstract class Struct<SerializedAs = unknown>
  implements Serializable<SerializedAs>, Validatable
{
  @Exclude()
  protected errors: ValidationError[] = []

  serialize(): SerializedAs {
    return instanceToPlain(this) as SerializedAs
  }

  isValid(): Promise<boolean> {
    return this.validate().then((errors) => !errors.length)
  }

  validate(): Promise<ValidationError[]> {
    return validate(this).then(
      tap<ValidationError[]>(
        (errors: ValidationError[]) => (this.errors = errors)
      )
    )
  }

  getValidationErrors() {
    return this.errors
  }
}
