import { Type } from '../../types/type.util'
import { FunctionReturns } from '../../types/function-returns.type'
import { ExtractFromType } from '../../types/instance-type'

export type Mixin<T = unknown> = (baseClass?: Type<any>) => Type<T>
export type ExtractFromMixin<T extends Mixin<any>> = ExtractFromType<
  FunctionReturns<T>
>

export class MixinArtisan {
  public static craft<T>(MixableType: Type<T>) {
    return (baseClass?: Type<object>) => {
      const constructorName = `${MixableType.name}Mixable`

      const Base = baseClass || class BaseClass {}

      const ClassStore = {
        [constructorName]: class extends Base {
          constructor() {
            super()
            if (typeof (this as any)[MixableType.name] === 'function') {
              ;(this as any)[MixableType.name]()
            }
          }
        },
      }

      const FinalClass: Type<any> = ClassStore[constructorName]

      const nonTransportableProperties = ['constructor']

      Object.getOwnPropertyNames(MixableType.prototype)
        .filter((p) => !nonTransportableProperties.includes(p))
        .forEach((name) => {
          FinalClass.prototype[name] = MixableType.prototype[name]
        })

      return ClassStore[constructorName] as Type<T>
    }
  }
}
