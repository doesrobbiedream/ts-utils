import { MaybeExpectedException } from './maybe-expected.exception'
import { Type } from '../../types/type.util'
import { isNullOrUndef } from '../../utility-functions/is-null-or-undef'
import { isOfType } from '../../utility-functions/is-of-type'

export class Maybe<T = unknown> {
  private readonly _value: T

  protected constructor(value: T) {
    this._value = value
  }

  public static just<T>(value: T) {
    return new this<T>(value)
  }

  public static nothing() {
    return new this(null)
  }

  public extract(): T {
    return this._value
  }

  public default<D>(value: D): Maybe<D | T> {
    return this.isNothing() ? Maybe.just(value) : this
  }

  public isNothing(): boolean {
    return isNullOrUndef(this._value)
  }

  public matchesType(type: Type): boolean {
    return isOfType(type)(this._value)
  }

  public expect(message = 'Expected value is not defined'): Maybe<T> {
    if (this.isNothing()) {
      throw new MaybeExpectedException(message)
    }
    return Maybe.just(this._value)
  }

  public expectOfType(type: Type, message?: string): Maybe<T> {
    message =
      message || `Expected value is not defined or is not of type ${type}`
    if (this.isNothing() || !this.matchesType(type)) {
      throw new MaybeExpectedException(message)
    }
    return Maybe.just<T>(this._value)
  }

  public map<R>(transformer: (x: T) => R): Maybe<R> | Maybe<null> {
    if (isNullOrUndef(this._value)) {
      return Maybe.nothing()
    }
    return isNullOrUndef(this._value)
      ? Maybe.nothing()
      : Maybe.just<R>(transformer(this._value))
  }
}
