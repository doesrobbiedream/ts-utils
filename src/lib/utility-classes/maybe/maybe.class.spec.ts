import { Maybe } from './maybe.class'

describe(Maybe, function () {
  describe('when value is defined', function () {
    it('should extract value', () => {
      const maybeSomething = Maybe.just('I am a string')
      expect(maybeSomething.extract()).toEqual('I am a string')
    })
  })
  describe('when value is defined & type is expected', function () {
    describe('and value is of correct type', function () {
      const maybeSomeNumber = Maybe.just(5)
      expect(() => maybeSomeNumber.expect()).not.toThrow()
      expect(maybeSomeNumber.expect().extract()).toEqual(5)
    })
    describe('and value is not of correct type', function () {
      it('should throw an error', () => {
        const maybeSomeNumberThatIsNotThere = Maybe.just(undefined)
        expect(() => maybeSomeNumberThatIsNotThere.expect().extract()).toThrow()
      })
    })
  })
  describe('when mapping a value to its correct type', function () {
    it('should extract the value', () => {
      const someOtherValue = Maybe.just(150)
      const theActualValue = someOtherValue.map((price) => '$' + price)
      expect(theActualValue.expectOfType(String).extract()).toEqual('$150')
    })
  })
  describe('when mapping a value to the incorrect type', function () {
    it('should throw an error', () => {
      const someOtherValue = Maybe.just(150)
      const theActualValue = someOtherValue.map((price) => '$' + price)
      expect(() => theActualValue.expectOfType(Number).extract()).toThrow()
    })
  })
  describe('when providing a default value', function () {
    describe('and value is defined', function () {
      const maybeWithDefault = Maybe.just('someValue').default('-')
      it('should extract the original value', () => {
        expect(maybeWithDefault.extract()).toEqual('someValue')
      })
    })
    describe('and value is not defined', function () {
      const maybeWithDefault = Maybe.just(undefined).default('-')
      it('should extract the default value', () => {
        expect(maybeWithDefault.extract()).toEqual('-')
      })
      it('should not throw an error', () => {
        expect(() => maybeWithDefault.expect()).not.toThrow()
      })
    })
  })
})
